#include<stdio.h>

int main(void)
{
	int x = 100;
	while (x % 3 != 0 || x % 4 != 0)
	{
		if (x % 3 != 0 && x % 4==0)
		{
			printf("%i is divisible by 4 but not by 3.\n",x);
		}
		else if (x % 3 == 0 && x % 4 !=0)
		{
			printf("%i is divisible by 3 but not by 4.\n",x);
		}
		else if (x % 3 != 0 && x % 4 !=0)
		{
			printf("%i is not divisible by 3 or 4.\n",x);
		}
		x++;
	}
	printf("%i is the first number over 100 that is divisible by 3 AND 4!\n",x);
	return 0;
}